<style>
    .banner {
        min-height: 60vh;
        background-image: url('<?= BASEURL; ?>/img/destination12.jpg');
        background-size: cover;
        background-position: center;
    }
</style>
<div class="container-fluid banner w-100 h-100 p-3 mx-auto text-center d-flex justify-content-center align-items-center text-white">
    <div class="container text-center">
        <h4 class="display-6">Selamat Datang di Website Saya</h4>
        <h3 class="display-1">Hai, Welcome</h3>
    </div>
</div>
<div class="container my-5">
    <div class="row">
        <div class="col-md-6">
            <h2>Our Story</h2>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis, delectus atque nobis eaque iure quam suscipit nulla ex optio impedit, qui, iste doloremque quo amet.
            </p>
            <p>
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequatur modi tempore architecto! Suscipit ipsa expedita consequatur harum laborum quidem ex voluptatem, cumque asperiores in labore possimus maxime dolor nulla dignissimos hic eos voluptates aliquam provident beatae, vitae reiciendis quibusdam cum libero. Ab, consequuntur minus! Facere quo reprehenderit aspernatur culpa voluptatum. </p>
        </div>
        <div class="col-md-6">
            <img src="<?= BASEURL; ?>/img/wisatabali.webp" class="card-img-top" alt="">
        </div>
    </div>
</div>
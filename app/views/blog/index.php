<!-- <div class="container mt-5">
    <div class="row">
        <div class="col-6"> -->
<!-- <h3>Blog</h3> -->
<!-- php foreach ($data["blog"] as $blog) : ?> -->
<!-- <ul> -->
<!-- <li>$blog["penulis"]; ?></li> -->
<!-- <li>$blog["judul"]; ?></li> -->
<!-- <li>$blog["tulisan"]; ?></li> -->

<!-- </ul> -->
<!-- php endforeach;  -->
<!-- <h3>Blog</h3>
            <ul class="list-group">
                php foreach ($data["blog"] as $blog) : ?> -->
<!-- <li class="list-group-item d-flex justify-conten-between align-items-center"><?= $blog['penulis']; ?>
                        = $blog['penulis']; ?>
                        <a href="<? BASEURL; ?>/blog/detail<?= $blog['id']; ?>" class="badge-primary">detail</a>
                    </li> -->
<!-- php endforeach;  -->
<!-- </ul>
        </div>
    </div>
</div>  -->


<header class="bg-primary text-light text-center p-5">
    <h1>Welcome to Our Tour Blog</h1>
</header>

<div class="container my-5">
    <div class="row">
        <div class="col-md-8">
            <div class="card mb-4">
                <img src="<?= BASEURL; ?>/img/wisata4.jpg" class="card-img-top" alt="">

                <div class="card-body">
                    <h5 class="card-title">Beautiful Destination</h5>
                    <p class="card-text">Bali is a tropical paradise located in Indonesia, known as one of the most beautiful destinations in the world. This island offers a variety of natural beauty, rich culture and environmental preservation.
                        Kelingking Beach is one of the most stunning coastal destinations in Bali. Located in the Nusa Penida area, this beach has become iconic thanks to its extraordinary views.
                    </p>
                    <a href="#" class="btn btn-primary">Read More</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card mb-4">
                <img src="<?= BASEURL; ?>/img/destination2.jpg" class="card-img-top" alt="">
                <div class="card-body">
                    <h5 class="card-title">Amazing Tour</h5>
                    <p class="card-text">Kelingking Beach locally known as Pantai Kelingking is one of the most iconic and recognisable tourist attractions in all of the Nusa islands.</p>
                    <a href="#" class="btn btn-primary">Read More</a>
                </div>
            </div>


            <div class="card mb-4">
                <img src="<?= BASEURL; ?>/img/destination6.jpg" class="card-img-top" alt="">
                <div class="card-body">
                    <h5 class="card-title">Enchanting Location</h5>
                    <p class="card-text">
                        Tirta Gangga Water Palace also known locally as Taman Tirta Gangga, is a former water palace of Karangasem empire which one of east Bali’s most famous sights. Situated about 6 kilometres north of the town of Amlapura, which feature 1.2ha of pools, ponds and fountains surrounded by tropical gardens.
                    </p>
                    <a href="#" class="btn btn-primary">Read More</a>
                </div>
            </div>
            <div class="card mb-4">
                <img src="<?= BASEURL; ?>/img/destination1.jpg" class="card-img-top" alt="">
                <div class="card-body">
                    <h5 class="card-title">Tropical Paradise</h5>
                    <p class="card-text">
                        Lake Beratan Tour is a journey into the extraordinary natural beauty of Bali, where you can explore the calm waters of the lake, while visiting the iconic temple Pura Ulun Danu Bratan which appears like a dream.
                    </p>
                    <a href="#" class="btn btn-primary">Read More</a>
                </div>
            </div>

        </div>

    </div>
</div>